import React, {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { activeModal, chariotSelect } from '../../slices/modalSlice';
import { Link,  withRouter } from 'react-router-dom';
import { hasUserSelect } from '../../slices/userSlice';
import HomeIcon from '../../Images/reciclo-home.png';
import HomeIconGreen from '../../Images/reciclo-home-turquoise.png';

import RecycleIcon from '../../Images/reciclo-pickup.png';
import RecycleIconGreen from '../../Images/reciclo-pickup-turquoise.png';

import DonationsIcon from '../../Images/reciclo-donations.png';
import DonationsIconGreen from '../../Images/reciclo-donations-turquoise.png';

import AssetsIcon from '../../Images/reciclo-assets.png';
import AssetsIconGreen from '../../Images/reciclo-assets-turquoise.png';

import './Navbar.scss';

const Navbar = (props) => {

    const hasUser = useSelector(hasUserSelect);
    const [ active, setActive ] = useState('Home');
    const chariot = useSelector(chariotSelect);
    const dispatch = useDispatch();

    const pickUpNavigation = (event) => {
        event.stopPropagation();
        event.preventDefault();
        if(chariot){
            dispatch(activeModal({origin: 'pickUpRecycle'}))
        }else{
            setActive('Pickup')
            props.history.push('/pickup');
        }
    }

    const goLogin = () => {
        props.history.push('/login');
    }

    return (
        <nav className="navbar">
            <div className="navbar__container">
                <span className="navbar__container__group">
              
                    <Link to="/">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Home' ? HomeIcon : HomeIconGreen}
                            alt="Home"
                            onClick={() => setActive('Home')}
                        />
                    </Link> 
                </span>
                <span className="navbar__container__group">
                {chariot && 
                    <div className="navbar__chariot" 
                        onClick="">

                 </div>}
                    {/* {hasUser && chariot && 
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Pickup' ? RecycleIcon : RecycleIconGreen}
                            alt="Pickup"
                            onClick={pickUpNavigation}
                        />}     */}
                    {chariot && <div className="navbar__chariot"></div>}
                    {hasUser && <Link to="/pickup">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Pickup' ? RecycleIcon : RecycleIconGreen}
                            alt="Pickup"
                            onClick={pickUpNavigation}
                        />
                    </Link>}
                    {!hasUser && <Link to="/login">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Pickup' ? RecycleIcon : RecycleIconGreen}
                            alt="Pickup"
                            onClick={hasUser ? () => setActive('Pickup') : ()=> goLogin}
                        />
                    </Link>}
                </span>

                <span className="navbar__container__group">
                    {hasUser && <Link to="/donate">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Donate' ? DonationsIcon : DonationsIconGreen}
                            alt="Donate"
                            onClick={() => setActive('Donate')}
                        />
                    </Link>}
                    {!hasUser && <Link to="/donate">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Donate' ? DonationsIcon : DonationsIconGreen}
                            alt="Donate"
                            onClick={() => setActive('Donate')}
                        />
                    </Link>}

                </span>

                <span className="navbar__container__group">
                    <Link to="/login">
                        <img     
                            className="navbar__container__group__icon"
                            src={active === 'Login' ? AssetsIcon : AssetsIconGreen}
                            alt="Login"
                            onClick={() => setActive('Login')}
                        />
                    </Link>
                </span>
            </div>
        </nav>
    )
}

export default withRouter(Navbar);