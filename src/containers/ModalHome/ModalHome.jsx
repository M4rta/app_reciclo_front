import React, { useState } from 'react';
import ModalButton from '../../Images/modalHome-button.png';
import Flower from '../../Images/reciclo-flor.png';
import CloseButton from '../../Images/button-close-dropdown.png';
import './ModalHome.scss';
import Graphic from '../Graphic';
import { useSelector } from 'react-redux';
import { hasUserSelect } from '../../slices/userSlice';
import Maps from '../Maps'
import LocationSearchInput  from '../LocationSearchInput';

export default function ModalHome() {

    const hasUser = useSelector(hasUserSelect);
    const [ dropDown, setDropDown ] = useState(false);

    const closeWindow = () => {
        setDropDown(false);
    }

    return(
        
        <div className="modal">
            {hasUser && <img
                src={ModalButton}
                alt="Button"
                className="modal__svg"
                onClick={() => setDropDown({dropDown: true})}
            />}
            {dropDown &&
                (
                <div className="modal__window">
                    <div className="modal__window__title">
                        <h2 className="modal__window__title__h2">Tus aportaciones</h2>
                        <h3 className="modal__window__title__h3">Marzo 2021</h3>
                    </div>
                    <div className="modal__window__diagram">
                        <Graphic/>   
                    </div>

                    <div className="modal__window__list">
                        <h3 className="modal__window__list__h3">Última recogida:</h3>
                        <ul className="modal__window__list__ul">
                            <li className="modal__window__list__ul__li">
                                10Kg de cartón
                            </li>
                            <li className="modal__window__list__ul__li">
                                5Kg de vidrio
                            </li>
                        </ul>
                    </div>

                    <div className="modal__window__list2">
                        <h2 className="modal__window__list2__h2">Impacto
                                <img src={Flower} alt="Flor" className="modal__window__list2__h2__img"/>
                        </h2>
                        <ul className="modal__window__list2__ul">
                            <li className="modal__window__list2__ul__li">
                                ¡Has reciclado suficiente cartón como para <span className="modal__window__list2__ul__li__span">imprimir un libro de 200 páginas!</span>
                            </li>
                            <li className="modal__window__list2__ul__li">
                                ¡Has reciclado suficiente vidrio como para <span className="modal__window__list2__ul__li__span">cargar tu smartphone durante 2 años!</span> 
                            </li>
                        </ul>
                    </div>

                    <div className="modal__window__images">
                        <div className="modal__window__images__images2">
                            <img src="https://visualhunt.com/photos/2/eyeglasses-on-open-book.jpg?s=s"
                            alt="Book"
                            className="modal__window__images__images2__img"/>
                        </div>

                        <div className="modal__window__images__images2">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkOVJqwiTh1RB-Re_01HWiRfdVp-9p4Qk9Sg&usqp=CAU"
                            alt="Charging"
                            className="modal__window__images__images2__img"/>
                        </div>
                            
                    </div>
                    
                    <span className="modal__window__close-button"
                    onClick={closeWindow}>
                        <img src={CloseButton} alt="Close window" className="modal__window__close-button__button"/>
                    </span>
                </div>
                )
            }
        </div>
    )

}