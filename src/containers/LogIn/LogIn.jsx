import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { hasUserSelect, setUserSlice } from "../../slices/userSlice.js";
import { login, logout } from "../../services/auth.js";
import logoIcon from "../../Images/reciclo-logo-08.svg";
import oIcon from "../../Images/reciclo-O.png";
import "./LogIn.scss";

export default function LogIn(props) {
  const dispatch = useDispatch();
  const [emailState, setEmail] = useState();
  const [passwordState, setPassword] = useState();
  const [errorState, setError] = useState();
  const hasUser = useSelector(hasUserSelect);

  const validate = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  };

  const goLogout = async () => {
    try {
      await logout();
      dispatch(setUserSlice({ user: null, hasUser: false }));
      props.history.push("/");
    } catch (error) {
      console.log(error);
    }
  };
  const handleSubmitForm = async (event) => {
    event.preventDefault();
    try {
      const validEmail = validate(emailState);
      if (!validEmail) {
        setError(
          "❌ Email no válido. Por favor, introdúzcalo con los parámetros correctos."
        );
      } else {
        const user = {
          email: emailState,
          password: passwordState,
        };
        const data = await login(user);
        dispatch(setUserSlice({ user: data, hasUser: true }));
        props.history.push("/");
      }
    } catch (error) {
      setError("email or password incorrect");
    }
  };

  const handleChangeInput = async (event) => {
    const { name, value } = event.target;
    if (name === "email") {
      setEmail(value);
    }
    if (name === "password") {
      setPassword(value);
    }
  };
  const goRegister = () => {
    props.history.push("/register");
  };

  return (
    <div className="container">
      <h1 className="container__h2">¡Bienvenid@ a Reciclo!</h1>
      <form onSubmit={handleSubmitForm} className="container__form">
        <label htmlFor="email" className="container__form__label">
          <input
            className="container__form__input"
            type="text"
            name="email"
            value={emailState}
            onChange={handleChangeInput}
            required
            placeholder="Correo electrónico"
          />
        </label>
        {errorState && <p className="register__form__error">{errorState}</p>}
        <label htmlFor="password" className="container__form__label">
          <input
            className="container__form__input"
            type="password"
            name="password"
            value={passwordState}
            onChange={handleChangeInput}
            required
            placeholder="Contraseña"
            className="container__form__input"
          />
        </label>
        <div>
          <button type="submit" className="container__form__button">
            ¡CONECTAR!
          </button>
        </div>
      </form>
      {!hasUser ? (
        <h3 className="register__link" onClick={goRegister}>
          Registrarse
        </h3>
      ) : (
        <h3 className="register__link" onClick={goLogout}>
          Log out
        </h3>
      )}

      <div className="container__icon-reciclo">
        <img src={logoIcon} alt="Logo-Reciclo" className="container__icon-reciclo__icon-recicl" />
        <img src={oIcon} alt="Logo-O" className="container__icon-reciclo__icon2" />
      </div>
    </div>
  );
}
