import React, { Component, useEffect } from 'react';
import { GoogleMap, LoadScript, Marker, Circle } from '@react-google-maps/api';
import { useSelector } from 'react-redux';
import { directionSelect } from '../../slices/pickUpSlice';
import './Maps.scss';
const containerStyle = {
  width: '90%',
  height: '550px',
  
};

// const center = {
//   lat: 40.292084,
//   lng: -3.809524
// };

const Maps = (props) => {

    const pickUp = useSelector(directionSelect);
    const hasUser = useSelector(state => state.user.hasUser)
    const { longitude, latitude } = pickUp;

    const closeMap = (event) => {
      event.preventDefault();
      props.history.push('/')
    }
    
    const center = {
      lat: latitude,
      lng: longitude
    };
    if(hasUser === null){
      return <div>
          ...loading
      </div>
  }
  if(hasUser === false){
      props.history.push('/');
  }
    return (
      <>
        <LoadScript
          googleMapsApiKey="AIzaSyBWmsf2k7wkdC5_QdK6XHRrkqrozhZDsLA"
        >
        <div className="b-maps">
          <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={17}>
            <Marker
              position= {center} />
          </GoogleMap>
          <button className="b-button-map" 
            type="submit"
            onClick={closeMap}>Cerrar</button>
        </div>
      </LoadScript>
 
    </>
    )
}
export default Maps