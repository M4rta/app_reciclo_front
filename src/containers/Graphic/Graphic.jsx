import { useEffect, useState } from 'react';
import {Bar} from 'react-chartjs-2';
import { useSelector } from 'react-redux';
import { getRecycleService } from '../../services/user';
import { userSelect } from '../../slices/userSlice';

const Graphic = () => {

    const [batteriesState, setBatteries] = useState();
    const [oilState, setOil] = useState();
    const [paperState, setPaper] = useState();
    const [clothesState, setClothes] = useState();
    const [glassState, setGlass] = useState()
    const data = {
        labels: ['Batteries', 'Oil', 'Paper', 'Clothes', 'Glass'],
        datasets: [
          {
            label: 'Mis aportaciones',
            backgroundColor:
                ['rgba(255,99,132,0.6)',
                'rgba(255,255,132,0.6)',
                'rgba(55,255,1,0.6)',
                'rgba(2,255,132,0.6)',
                'rgba(255,2,132,0.6)'],
            borderColor: 
                ['rgba(255,99,132,1)',
                'rgba(255,255,132,1)',
                'rgba(55,255,1,1)',
                'rgba(2,255,132,1)',
                'rgba(255,2,132,1)'],
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [batteriesState, oilState, paperState, clothesState, glassState]
          }
        ]
      };

    const user = useSelector(userSelect);
    useEffect (() => {
        getRecycle();
    },[])

    const getRecycle = async() => {

        const data = await getRecycleService(user._id);
        addRecycleAmount(data);

    }

    const addRecycleAmount = (data) => {
        let batteries = 0;
        let clothes = 0;
        let glass = 0;
        let oil = 0;
        let paper = 0;
        data.forEach(element => {
            batteries += element.recicle.batteries;
            glass += element.recicle.glass;
            clothes += element.recicle.clothes;
            oil += element.recicle.oil;
            paper += element.recicle.paper;
        });
        setBatteries(batteries);
        setClothes(clothes);
        setGlass(glass);
        setOil(oil);
        setPaper(paper);

    }
    return (
        
      <div>
        {/* <h2>Bar Example (custom size)</h2> */}
        <Bar
          data={data}
          width={200}
          height={150}
          options={{
            maintainAspectRatio: false
          }}
        />
      </div>
    );
  }

  export default Graphic