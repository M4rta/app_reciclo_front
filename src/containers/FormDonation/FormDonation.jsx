import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import GoBack from "../../Images/reciclo-goBack.png";
import Earth from "../../Images/reciclo_earth.png";
import { useSelector, useDispatch } from 'react-redux';
import { makeDonation, hasDonated} from '../../services/donations';
import { activeModal } from '../../slices/modalSlice';
import { saveDonation } from '../../slices/donationSlice';
import { sendingEmail } from '../../services/email';
import "./FormDonation.scss";
// import { isAsyncThunkAction } from "@reduxjs/toolkit";

const INITIAL_STATE = {
  money:'',
  username:'',
  lastName:'',
  email:'',
  postalCode:'',
  poblation:'',
  creditNumber:'',
  creditDate:'',
  creditYear:'',
  cvv:'',
}

export default function FormDonation(props) {

  const dispatch = useDispatch();
  const history = useHistory();
  const donationId = useSelector(state => state.donation.donation._id);
  const [form, setForm] = useState (INITIAL_STATE);
  if(!donationId) history.push("/donate");

  const handleHistory = () => {
    history.push("/donate");
  };

  const submitForm = async (event) =>{
    event.preventDefault();
  
    const createEmail = (data) =>{
      return {...data, form}
    }
    try {
      const marta = await makeDonation(form.money, donationId);
      const email = createEmail(marta)
      await sendingEmail(email)
      dispatch(saveDonation(marta));
      dispatch(activeModal({origin: "donation"}));
    }catch(error) {
      console.log(error);
    }
  }

  const handleChangeform = (event) =>{
      const {value, name} = event.target;

      setForm({...form, [name]: value});
  } 

  return (
    <div className="form-donation">
      <div className="form-donation__btn-goback">
        <button onClick={handleHistory} className="donate__btn-goback__btn">
          <span>
            <img
              src={GoBack}
              alt="Atrás"
              className="donate__btn-goback__btn__span"
            />
          </span>
          Atrás
        </button>
      </div>

      <h2 className="form-donation__h2">
        <span>
          <img src={Earth} alt="Tierra" className="form-donation__h2__span" />
        </span>
        Indica tu donativo
      </h2>

      {/* FORMULARIO */}
      <form className="form-donation__form" onSubmit={submitForm}>
        <div className="form-donation__form__money">
          <label className="form-donation__form__money__label">
            <input
              type="number"
              name="money"

              value={form.money}
              onChange= {handleChangeform}
              maxlength="5"
              className="form-donation__form__money__label__input"
            />
            <span className="form-donation__form__money__label__span€">€</span>
          </label>
        </div>

        <label htmlFor="username" className="form-donation__form__label">
          <input
            type="text"
            name="username"
            placeholder="Nombre"
            value={form.username}
            onChange= {handleChangeform}
            className="form-donation__form__input"
          />
        </label>

        <label htmlFor="lastName" className="form-donation__form__label">
          <input
            type="text"
            name="lastName"
            placeholder="Apellidos"
            value={form.lastName}
            onChange= {handleChangeform}
            className="form-donation__form__input"
          />
        </label>

        <label htmlFor="email" className="form-donation__form__label">
          <input
            type="text"
            name="email"
            value={form.email}
            onChange= {handleChangeform}
            placeholder="Correo electrónico"
            className="form-donation__form__input"
          />
        </label>

        {/* {errorState && <p className="register__form__error">{errorState}</p>} */}

        <div className="form-donation__form__many">
          <label
            htmlFor="postalCode"
            className="form-donation__form__many__label"
          >
            <input
              type="number"
              name="postalCode"
              value={form.postalCode}
              onChange= {handleChangeform}
              placeholder="C.P."
              className="form-donation__form__many__label__input-cd"
            />
          </label>

          <label
            htmlFor="poblation"
            className="form-donation__form__many__label"
          >
            <input
              type="text"
              name="poblation"
              value={form.poblation}
              onChange= {handleChangeform}
              placeholder="Población"
              className="form-donation__form__many__label__input-poblation"
            />
          </label>
        </div>

        <label htmlFor="credit-number" className="form-donation__form__label">
          <input
            type="number"
            name="creditNumber"
            placeholder="Nº tarjeta de crédito"
            value={form.creditNumber}
            onChange= {handleChangeform}
            maxLength="16"
            className="form-donation__form__input"
          />
        </label>

        <div className="form-donation__form__many">
          <label htmlFor="credit-date" className="form-donation__form__many__label">
            <input
              type="number"
              name="creditDate"
              placeholder="00"
              value={form.creditDate}
              onChange= {handleChangeform}

              maxLength="2"
              className="form-donation__form__many__label__input-date"
            />
          </label>
          <label htmlFor="credit-year" className="form-donation__form__many__label">
            <input
              type="number"
              name="creditYear"
              placeholder="0000"
              value={form.creditYear}
              onChange= {handleChangeform}

              maxLength="4"
              className="form-donation__form__many__label__input-year"
            />
          </label>

          <label htmlFor="credit-CVV" className="form-donation__form__many__label">
            <input
              type="number"
              name="cvv"
              placeholder="CVV"
              value={form.cvv}
              onChange= {handleChangeform}

              maxLength="3"
              className="form-donation__form__many__label__input-cvv"
            />
          </label>
        </div>

        <div>
          <button type="submit" className="form-donation__form__button">
            Donar
          </button>
        </div>
      </form>
    </div>

    //en el onSubmit tiene que llamar a un servicio y pasar el amount y la id
    //de la donación.

    // http://localhost:4000/donations/donate/200/602ab6af5fb1d73f14b26a7d

    
  );
}
