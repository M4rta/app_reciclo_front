import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getDonationsService } from '../../services/donations';
import { saveDonation } from '../../slices/donationSlice';
import { hasUserSelect } from '../../slices/userSlice';
import { getDonationsByIdService } from '../../services/donations';
import { withRouter } from 'react-router-dom';
import './Carrusel.scss';

const Carrusel = (props) => {

    const hasUser = useSelector(hasUserSelect);
    const dispatch = useDispatch();
    const [allDonations, setAllDonations] = useState([]);
    useEffect(() => {
        downloadDonations();
     }, []);

     const downloadDonations = async() => {
    try{
        const donations = await getDonationsService();
        setAllDonations(donations);
    }catch(error){
        }
    }
    
    const showdetail = async (event) => {
        if (hasUser === false){
            props.history.push('/login')
        }else{
        const { id } = event.target;
        const data = await getDonationsByIdService(id);
        dispatch(saveDonation(data));
        props.history.push('/donate');
        }
    }
    return(
        <div>
            <h2 className="title-carousel">Noticias y donaciones</h2>
            <div className="b-carrousel">
            {allDonations.length > 0 && allDonations.map( data => (
                <div className="b-carrousel__card" key={data._id}>
                    <div  className="test">
                        <img id={data._id} src={data.photo} alt={data.alt} onClick={showdetail} className="b-carrousel__image"/>
                        <h3 className="b-carrousel__divphrase">{data.title}</h3>
                    </div>
                 </div>))}
            </div>
        </div>
    )
}
export default withRouter(Carrusel);