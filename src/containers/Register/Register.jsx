import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../../services/auth.js';
import { userSelect, setUserSlice } from '../../slices/userSlice.js';
import './Register.scss';

export default function Register(props) {

    const dispatch = useDispatch();
    const user = useSelector(userSelect)

    const [emailState, setEmail] = useState();
    const [usernameState, setUserName] = useState();
    const [lastnameState, setLastname] = useState();
    const [passwordState, setPassword] = useState();
    const [password2State, setPassword2] = useState();
    const [phoneState, setPhone] = useState();
    const [pdState, setPd] = useState();
    const [streetState, setStreet] = useState();
    const [poblationState, setPoblation] = useState();
    const [errorState, setError] = useState();

    const validate = (email) => {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
      };

    const handleSubmitForm = async(event) => {
        event.preventDefault();
        try {
            const validEmail = validate(emailState);
            if (!validEmail) {
              setError("❌ Email no válido. Por favor, introdúzcalo con los parámetros correctos.");
            }else if (passwordState === password2State) {
                const user = {
                email: emailState,
                username: usernameState,
                lastName: lastnameState,
                password: passwordState,
                phone: phoneState,
                // postalCode: pdState,
                // street: streetState,
                // poblation: poblationState,
                }
                const data = await register(user);
                if(data.name === 'Error'){
                    setError("El usuario ya existe");
                }else{
                dispatch(setUserSlice({user: data, hasUser: true}));
                props.history.push("/");
                }
            } 
            else{
                setError("Las contraseñas no coinciden");
            }
        }catch (error) {
            console.log(error);
        }
    }

    const handleChangeInput = async (event) => {
        const { name, value } = event.target;

        if(name === 'email'){
            setEmail(value);
        }
        if(name === 'username'){
            setUserName(value);
        }
        if(name === 'lastName'){
            setLastname(value);
        }
        if(name === 'password'){
            setPassword(value);
        }
        if(name === 'password2'){
            setPassword2(value);
        }
        if(name === 'phone'){
            setPhone(value);
        }
        // if(name === 'postalCode'){
        //     setPd(value);
        // }
        // if(name === 'street'){
        //     setStreet(value);
        // }
        // if(name === 'poblation'){
        //     setPoblation(value);
        // }
    };

    return (
        <div className="register">
            {/* <span><img src="form-25.svg" alt="form"/></span> */}
            <h1 className="register__h2">Formulario de registro</h1>
            <form onSubmit={handleSubmitForm} className="register__form">

                <label  htmlFor="username" className="register__form__label">
                    <input type="text" name="username" value={usernameState} onChange={handleChangeInput} required placeholder="Nombre" className="register__form__input"/>
                </label>

                <label  htmlFor="lastName" className="register__form__label">
                    <input type="text" name="lastName" value={lastnameState} onChange={handleChangeInput} placeholder="Apellidos" className="register__form__input"/>
                </label>

                <label  htmlFor="email" className="register__form__label">
                    <input type="text" name="email" value={emailState} onChange={handleChangeInput} required placeholder="Correo electrónico" className="register__form__input"/>
                </label>

                {errorState && <p className="register__form__error">{errorState}</p>}

                <label  htmlFor="password" className="register__form__label">
                    <input type="password" name="password" value={passwordState} onChange={handleChangeInput} required placeholder="Contraseña" className="register__form__input"/>
                </label>

               <label  htmlFor="password" className="register__form__label">
                    <input type="password" name="password2" value={password2State} onChange={handleChangeInput} required placeholder="Repita contraseña" className="register__form__input"/>
                </label>

                {/* {errorState && <p className="register__form-register__error">{"❌ Ups, ¡las contraseñas no coinciden!"}</p>} */}

                <label  htmlFor="phone" className="register__form__label">
                    <input type="number" name="phone" value={phoneState} onChange={handleChangeInput} placeholder="Teléfono" className="register__form__input"/>
                </label>

               {/* <label  htmlFor="postalCode" className="register__form__label">
                    <input type="number" name="postalCode" value={pdState} onChange={handleChangeInput} placeholder="Código Postal" className="register__form__input"/>
                </label>

                <label  htmlFor="street" className="register__form__label">
                    <input type="text" name="street" value={streetState} onChange={handleChangeInput} placeholder="Calle" className="register__form__input"/>
                </label>

                <label  htmlFor="poblation" className="register__form__label">
                    <input type="text" name="poblation" value={poblationState} onChange={handleChangeInput} placeholder="Población" className="register__form__input"/>
                </label> */}

                <div>
                    <button type="submit" className="register__form__button">¡Regístrate!</button>
                </div>
            </form>
        </div>
    )
}
