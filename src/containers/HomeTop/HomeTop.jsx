
import React, {useState} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import logoAll from '../../Images/reciclo-logo-home-27.svg';
import profilePhoto from '../../Images/reciclo-profie-photo-28.svg';
import { addPhotoService } from '../../services/user';
import { setUserSlice, userSelect, hasUserSelect } from '../../slices/userSlice';
import './HomeTop.scss';

export default function HomeTop(props) {

    const user = useSelector(userSelect);
    const hasUser = useSelector(hasUserSelect);
    const [profileImg, setProfileImage] = useState(profilePhoto);
    const [photoState, setPhoto] = useState(null);
    const dispatch = useDispatch();

    const handle = (event) => {

        let reader = new FileReader();
        let file = event.target.files[0];
     
        reader.onloadend = () => {
            setPhoto(file)
            setProfileImage(reader.result)
            document.getElementById('boton').click();
        }
        reader.readAsDataURL(file)
    }

    const submitImageHandler = async (ev) =>{
        ev.preventDefault();
        const addPhoto = new FormData();
        addPhoto.append('photo', photoState);
        const data = await addPhotoService(addPhoto);
        dispatch(setUserSlice({user: data}))
    }

    return(
        <div className="top-container">
            <form id="add-photo" onSubmit={submitImageHandler} className="top-container__divPhoto" method="POST" encType="multipart/form-data">
                <label className="top-container__label" htmlFor="photo">
                    <img src={user && user.photo ? user.photo : profileImg} 
                        onClick={() => hasUser ? document.getElementById('add-photo-input').click() : ()=>props.history.push('/')} 
                        alt="default" 
                        className={user && user.photo ? "top-container__divPhoto__img top-container__divPhoto__img--glow" : "top-container__divPhoto__img"}/>
                    <input id="add-photo-input" type="file" 
                        name="photo"
                        onChange={handle}
                        visibility="hidden"
                        style={{visibility: 'hidden'}}
                        accept="image/png, image/jpg, image/jpeg"/>
                </label>
                <button id="boton" className="top-containner__button" style={{visibility: 'hidden'}} type="submit"></button>
            </form>
            <div className="top-container__divUser">
            <img src={logoAll}
                alt="Reciclo"
                className="top-container__divUser__logo"/>
                <h3 className="top-container__divUser__h3">¡Bienvenid@ {user && user.username}!</h3>
            </div>
        </div>
    )

}