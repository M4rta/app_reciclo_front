// const userUrl = 'http://localhost:4000/auth';
// const pickUpUrl = 'http://localhost:4000/pickup';
const userUrl = 'https://node-auth-upgrade-daniel.herokuapp.com/auth';
const pickUpUrl = 'https://node-auth-upgrade-daniel.herokuapp.com/pickup';

export const addDirectionService = async (direction) => {
  const request = await fetch(`${userUrl}/addDirection`, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(direction),
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}

export const addPhotoService = async photo => {
  const request = await fetch(`${userUrl}/addphoto`, {
    method: "POST",
    headers: {
      "Accept": "*",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: photo,
})

const response = await request.json();
if(!request.ok) {
  return new Error(response.message);
}
return response;
}

export const getRecycleService = async id => {
  const request = await fetch(`${pickUpUrl}/allpickups`, {
    method: "GET",
    headers: {
      "Accept": "*",
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
})

const response = await request.json();
if(!request.ok) {
  return new Error(response.message);
}
return response;
}

export const savePickUpService = async (pickUp) => {
  const request = await fetch(`${userUrl}/form`, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
    body: JSON.stringify(pickUp),
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}
