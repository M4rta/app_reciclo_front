// const donationUrl = 'http://localhost:4000/donations';
const donationUrl = 'https://node-auth-upgrade-daniel.herokuapp.com/donations';

export const getDonationsService = async () => {
  const request = await fetch(`${donationUrl}/information`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}

export const getDonationsByIdService = async (id) => {
  const request = await fetch(`${donationUrl}/getdonate/${id}`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}

export const makeDonation = async (amount, idDonation) => {
  const request = await fetch(`${donationUrl}/donate/${amount}/${idDonation}`,
   {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}

export const hasDonated = async (id) => {
  const request = await fetch(`${donationUrl}/hasdonated/${id}`, {
    method: "GET",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Credentials": true,
      "Access-Control-Allow-Origin": "*",
    },
    credentials: 'include',
  })
  const response = await request.json();
  if(!request.ok) {
    return new Error(response.message);
  }
  return response;
}