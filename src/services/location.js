import axios from 'axios';

export const LocationSearchInput = (query) => {
    const access_key = 'c50f3d3c431ccacb3270909ba1c9a39f';
    const request = axios.get(`http://api.positionstack.com/v1/forward?access_key=${access_key}&query=${query}`)
    return request
        .then(response => {
            return response.data
    }).catch(error => {
            console.log(error);
    })
}
