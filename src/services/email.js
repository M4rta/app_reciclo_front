// const utilsURL= 'http://localhost:4000/utils/email';
const utilsURL= 'https://node-auth-upgrade-daniel.herokuapp.com/utils/email';

export const sendingEmail = async (email) => {
    const request = await fetch(`${utilsURL}`,  { 
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Credentials": true,
    },
    credentials: 'include',
    body: JSON.stringify(email),
    })
    const response = await request.json();
    if(!request.ok) {
      return new Error(response.message);
    }
    return response;
}