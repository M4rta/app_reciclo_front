import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App.jsx';
import store from './app/store';
import { Provider } from 'react-redux';

const Main = () => {

  return (<React.StrictMode>
            <Router>
              <Provider store={store}> 
                <App />
              </Provider>
            </Router>
          </React.StrictMode>)
} 
ReactDOM.render(
  <Main/>,
  document.getElementById('root')
);

