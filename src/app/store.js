import { configureStore } from '@reduxjs/toolkit';
import pickUpReducer from '../slices/pickUpSlice'
import userReducer from '../slices/userSlice';
import modalRedcuer from '../slices/modalSlice';
import donationReducer from '../slices/donationSlice';
export default configureStore({
  reducer: {
    pickUp: pickUpReducer,
    user: userReducer,
    modal: modalRedcuer,
    donation: donationReducer
  },
});
