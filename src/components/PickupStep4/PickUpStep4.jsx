import { useSelector } from 'react-redux';
import { dateSelect, hourSelect, minuteSelect, recycleSelect, directionSelect} from '../../slices/pickUpSlice';
import { userSelect } from '../../slices/userSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCalendarAlt, faCarBattery, faClock, faMapMarkerAlt, faOilCan, faRecycle, faScroll, faTshirt, faWineBottle} from '@fortawesome/free-solid-svg-icons';
import './PickUpStep4.scss';

const PickUpStep4 = () => {

    const date = useSelector(dateSelect);
    const hour = useSelector(hourSelect);
    const minute = useSelector(minuteSelect);
    const recycle = useSelector(recycleSelect);
    const direction = useSelector(directionSelect);

    const {label } = direction;
    const {paper, oil, clothes, batteries, glass } = recycle;
    const day = date.getUTCDate();
    const month = date.getUTCMonth();
    const year = date.getUTCFullYear();

    return(
        <div b-pickup__step4>
            <p className="b-phrase">¿Todos los datos están correctos?</p>
            <div>
                <div className="b-pickup__resume">
                     <FontAwesomeIcon  className="b-pickup__icon faCarBattery b-pickup__icon--resume" icon={faCalendarAlt} />
                     <p className="b-pickup__description">{day} / {month+1} / {year}</p>
                </div>
                <div className="b-pickup__resume">
                     <FontAwesomeIcon  className="b-pickup__icon b-pickup__icon--resume " icon={faClock} />
                     <p className="b-pickup__description">{hour}: {minute}</p>
                </div>
                <div className="b-pickup__resume">
                     <FontAwesomeIcon  className="b-pickup__icon b-pickup__icon--resume" icon={faMapMarkerAlt} />
                     <p className="b-pickup__description">{label}</p>
                </div>
                <div className="b-pickup__resume">
                     <FontAwesomeIcon  className="b-pickup__icon b-pickup__icon--resume__last" icon={faRecycle} />
                     <span>{glass > 0 && <FontAwesomeIcon className="b-pickup__icon b-pickup__icon--description" icon={faWineBottle} />} </span>
                     <span>{oil > 0 && <FontAwesomeIcon className="b-pickup__icon b-pickup__icon--description" icon={faOilCan} />}</span>
                     <span>{paper > 0 && <FontAwesomeIcon className="b-pickup__icon b-pickup__icon--description" icon={faScroll} />} </span>
                     <span>{clothes > 0 && <FontAwesomeIcon className="b-pickup__icon b-pickup__icon--description" icon={faTshirt} />} </span>
                     <span>{batteries> 0 && <FontAwesomeIcon className="b-pickup__icon b-pickup__icon--description" icon={faCarBattery} />}</span>
                </div>
            
            </div>
        </div>
       
    
    )
}

export default PickUpStep4;