import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { userSelect, setUserSlice } from '../../slices/userSlice';
import { addDirection } from '../../slices/pickUpSlice';
import { addDirectionService } from '../../services/user';
import { LocationSearchInput } from '../../services/location';
import './PickUpStep2.scss';

const PickUpStep2 = () => {

    const INITIAL_STATE = {
        street: '',
        postalCode: '',
        poblation: ''
    }


    const dispatch = useDispatch();
    const user = useSelector(userSelect);

    const [otherDirection, setOtherDirection] = useState(false);
    const [directionForm, setDirectionForm] = useState(INITIAL_STATE);
    const [direction, setDirection] = useState();

    const selectCheckbox = (event) => {
        const { value } = event.target;
        let selectedDirection = user.directionArray[value];
        dispatch(addDirection(selectedDirection))
    }

    const handleDirecctionForm = (event) => {
        const {value, name} = event.target;
        setDirectionForm({...directionForm, [name]: value});
    }

    const onSubmitForm = async(event) => {
        event.preventDefault();
        const sendDirection = {
            direction: directionForm,
            id: user._id
        }
        const { poblation, street, postalCode } = sendDirection.direction;
        const query = poblation + ', '+ street + ', '+ postalCode;
        const data = await LocationSearchInput(query);
        setOtherDirection(false)
        setDirection(data);
    }

    const chooseDirection = async(data) => {
        const sendDirection = {
            direction: data,
            id: user._id
        }
        const userFetch = await addDirectionService(sendDirection);
        dispatch(setUserSlice({user: userFetch, hasUser: true}));
        setDirection(null);
    }

    return(
        <div className="pickup_container">
             <p className="pickup_container__title">Selecciona la dirección para la recogida</p>
            <form className="" >
                <div className="b-pickup__array">
                {user && user.directionArray.map((direction, index) => {
                    return (
                        <div className="b-pickup__card" key={direction._id}>
                            <input className="b-pickup__radio" type="radio" name="direction" value={index} onClick={selectCheckbox}></input>
                            <p className="b-pickup__directions">{direction.label},{direction.postalCode}, {direction.poblation}</p>
                        </div>
                    )})
                    } 
                </div> 
            </form>
            <p className="b-pickup__add" 
                onClick={()=> setOtherDirection(true)}>
                Añadir otra dirección
            </p>
            {otherDirection && <div>
                <form className="b-pickup__addform" onSubmit={onSubmitForm}>
                    <label for="street-pickup"></label>
                    <input className="b-pickup__addform-input" 
                        id="street-pickup" type="text" 
                        onChange={handleDirecctionForm} 
                        name="street" 
                        placeholder="Dirección completa">
                    </input>
                    <button className="b-button" type="submit">Añadir</button>
                </form>
            </div>}
            <div>
            {direction && direction.data.map( data => (
                <div className="" key={data.latitude}>
                    <div  className="test">
                        <p className="direction" onClick={() => chooseDirection(data)}>{data.label}</p>
                        <h3 className="">{data.title}</h3>
                    </div>
                 </div>))}
            </div>
        </div>
    )
}

export default PickUpStep2;