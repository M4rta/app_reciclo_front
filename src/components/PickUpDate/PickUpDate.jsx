import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronUp, } from '@fortawesome/free-solid-svg-icons';
import { minuteSelect, hourSelect, addHoursSlice, addMinutesSlice } from '../../slices/pickUpSlice';
import './PickUpDate.scss';

const PickUpDate = (prop) => {
    const dispatch = useDispatch()

    const minute = useSelector(minuteSelect);
    const hour = useSelector(hourSelect);

    const addMinutes = (event) => {
        const {id} = event.currentTarget;
        dispatch(addMinutesSlice({id}))   
    }

    const addHours = (event) => {
        const {id} = event.currentTarget;   
        dispatch(addHoursSlice({id}))   
    }
    const getTwoNumbers = (number) => number < 10 ? `0${number}` : number;

    return(
        <div className="b-hour__container">
            <div className="b-hour__row">
                <span id="Add-hour" onClick={addHours}><FontAwesomeIcon  className="b-hour__icon" icon={faChevronUp} /></span>
                <input className="b-hour__hour" maxlength="2" size="2" type="number" name="hour" value={hour} />
                <span id="Sub-hour" onClick={addHours}><FontAwesomeIcon  className="b-hour__icon" icon={faChevronDown} /></span>          
            </div>
            <div className="b-hour__row">
                <span id="Add-minutes" onClick={addMinutes}><FontAwesomeIcon  className="b-hour__icon" icon={faChevronUp} /></span>
                <input className="b-hour__hour" maxlength="2" size="2" type="number" name="hour" value={getTwoNumbers(minute)} />
                <span id="Sub-minutes" onClick={addMinutes}><FontAwesomeIcon  className="b-hour__icon" icon={faChevronDown} /></span>                
            </div>
        </div>
    )
}

export default PickUpDate;