import './PickUpStep1.scss';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from "react-datepicker";
import PickUpDate from '../PickUpDate';
import { dateSelect, getCalendarSlice } from '../../slices/pickUpSlice';
import "react-datepicker/dist/react-datepicker.css";

const PickUpStep1 = () => {

    const dispatch = useDispatch();
    const dates = useSelector(dateSelect)

    return(
        <div className="b-pickUp__step1">
            <p className="b-pickup__title">Selecciona un día y una hora para tu recogida</p>
            <div className="b-pickUp__calendar">
                <DatePicker
                    inline
                    selected={dates} 
                    dateFormat="MMMM d, yyyy" 
                    onChange={date =>dispatch(getCalendarSlice(date))}
                    minDate={new Date()}/>
            </div>
            <PickUpDate  />
    </div>
    )
}

export default PickUpStep1;