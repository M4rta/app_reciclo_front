import "./DonateCard.scss";
import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { getDonationsService } from "../../services/donations";
import { BrowserRouter as Router, useHistory, Link } from "react-router-dom";
import GoBack from "../../Images/reciclo-goBack.png";
import {
  donationChooseSelect,
  donationSelect,
} from "../../slices/donationSlice";
import ButtonShadow from "../../Images/button-close-dropdown.png";

const DonateCard = (props) => {
  const donation = useSelector(donationSelect);
  const selectDonation = useSelector(donationChooseSelect);
  const { title, description, photo, target, collected } = donation;

  const history = useHistory();

  useEffect(() => {
    downloadDonations();
  }, []);

  const [allDonations, setAllDonations] = useState([]);
  const downloadDonations = async () => {
    try {
      const donations = await getDonationsService();
      setAllDonations(donations);
      console.log(donations);
    } catch (error) {}
  };

  const handleHistory = () => {
    history.push("/");
  };

  return (
      <div className="donate">
        <div className="donate__btn-goback">
          <button onClick={handleHistory} className="donate__btn-goback__btn">
            <span>
              <img
                src={GoBack}
                alt="Atrás"
                className="donate__btn-goback__btn__span"
              />
            </span>
            Atrás
          </button>
        </div>

        {selectDonation && (
          <div className="donate__card">
            <h2 className="donate__card__title">{title}</h2>
            <div className="donate__card__white">
              <img
                src={photo}
                alt="Imagen card"
                className="donate__card__white__img"
              />
              <p className="donate__card__white__description">{description}</p>
              <h3 className="donate__card__white__donation">
                <span className="donate__card__white__donation__span">
                  {collected}€
                </span>{" "}
                recaudados del objetivo de{" "}
                <span className="donate__card__white__donation__span">
                  {target}€
                </span>
              </h3>
              
                <div className="donate__card__white__button-donate">
                <Link to="/form-donation">
                  <button className="donate__card__white__button-donate__btn">
                    Donar ahora
                  </button>
                </Link>
                </div>
              
              <span className="donate__card__white__shadow-button">
                <img
                  src={ButtonShadow}
                  alt="Close window"
                  className="donate__card__white__shadow-button__btn"
                />
              </span>
            </div>
          </div>
        )}
      </div>
    
  );
};

export default DonateCard;