import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCarBattery, faOilCan, faScroll, faTshirt, faWineBottle} from '@fortawesome/free-solid-svg-icons';
import { addRecycle, recycleSelect } from '../../slices/pickUpSlice';
import './PickUpStep3.scss';
import { useEffect } from 'react';

const PickUpStep3 = () => {

    const INITIAL_STATE = {
        glass: 0,
        batteries: 0,
        clothes: 0,
        paper: 0,
        oil:0
    }

    const dispatch = useDispatch();
    const [recycle, setRecicle] = useState(INITIAL_STATE);
    const [error, setError] = useState();

    useEffect (()=>{
        // getRecycleState()
    },[])

    const allRecycle = useSelector(recycleSelect);

    const handleAmountForm = (event) => {
        const {value, name} = event.target;
        dispatch(addRecycle({...allRecycle, [name]: value }))
    }

    const onSubmitForm = (event) => {
        event.preventDefault();
        const {oil, paper, clothes, glass, batteries } = recycle;
        if((oil || paper || clothes || batteries || glass) > 0){
            dispatch(addRecycle(recycle));
        }else{
            setError('Tiene que enviar algo a reciclar');
        }
    }

    return(
        <div>
            <p className="b-title">¿Qué quieres reciclar?</p>
            {error && <p className="b-error">{error}</p>}
            <form className="b-pickup__recicle" onSubmit={onSubmitForm}>
               
                <div className="b-pickup__div">
                    <FontAwesomeIcon  className="b-pickup__icon" icon={faWineBottle} /><label className="b-pickup__label" for="recicle-glass"></label>
                    <input className="b-pickup__input" 
                        id="recicle-glass" 
                        maxlength="2" size="2" 
                        type="text" 
                        value={allRecycle.glass}
                        onChange={handleAmountForm} 
                        name="glass"></input>
                </div>
                <div className="b-pickup__div">
                    <FontAwesomeIcon  className="b-pickup__icon" icon={faCarBattery} /><label className="b-pickup__label" for="recicle-batteries"></label>
                    <input className="b-pickup__input" 
                        id="recicle-batteries" 
                        maxlength="2" 
                        size="2" 
                        type="text" 
                        value={allRecycle.batteries}
                        onChange={handleAmountForm} 
                        name="batteries"></input>
                </div>
                <div className="b-pickup__div">
                    <FontAwesomeIcon  className="b-pickup__icon" icon={faTshirt} /><label className="b-pickup__label" for="recicle-clothes"></label>
                    <input className="b-pickup__input" 
                        id="recicle-clothes" 
                        maxlength="2" 
                        size="2" 
                        type="text"
                        value={allRecycle.clothes}
                        onChange={handleAmountForm} 
                        name="clothes"></input>
                </div>
                <div className="b-pickup__div">
                    <FontAwesomeIcon  className="b-pickup__icon" icon={faScroll} /><label className="b-pickup__label" for="recicle-paper"></label>
                    <input className="b-pickup__input" 
                        id="recicle-paper" 
                        maxlength="2" 
                        size="2" 
                        type="text"
                        value={allRecycle.paper}
                        onChange={handleAmountForm} name="paper"></input>
                </div>
                <div className="b-pickup__div">
                    <FontAwesomeIcon  className="b-pickup__icon__last" icon={faOilCan} /><label className="b-pickup__label" for="recicle-oil"></label>
                    <input className="b-pickup__input" 
                        id="recicle-oil" 
                        maxlength="2" 
                        size="2" 
                        type="text" 
                        value={allRecycle.oil}
                        onChange={handleAmountForm} 
                        name="oil"></input>
                </div>
                {/* <div className="b-pickup__buttonAdd">
                    <button className="b-button" type="submit">Añadir</button>
                </div> */}
            </form>
        </div>
    )
}


export default PickUpStep3;