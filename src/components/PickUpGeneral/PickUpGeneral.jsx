import { useEffect, useState } from 'react';
// import { Route, Redirect } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft} from '@fortawesome/free-solid-svg-icons';
import Stepper from '../stepper';
import PickUpStep1 from '../PickUpStep1';
import PickUpStep2 from '../PickUpStep2';
import PickUpStep3 from '../PickUpStep3';
import PickUpStep4 from '../PickupStep4';
import { useSelector, useDispatch } from 'react-redux';
import './PickUpGeneral.scss';
import { activeModal, modalSelect } from '../../slices/modalSlice';
import { savePickUpService } from '../../services/user';
import { dateSelect, hourSelect, minuteSelect, recycleSelect, directionSelect } from '../../slices/pickUpSlice';
import { hasUserSelect } from '../../slices/userSlice';

const PickUpGeneral = (props) => {

    const hasUser = useSelector(state => state.user.hasUser)

    // const modal = useSelector(modalSelect);
    // const user = useSelector(hasUserSelect);

    const dispatch = useDispatch();
    const pickUp = {
        date : useSelector(dateSelect),
        hour : useSelector(hourSelect),
        minute: useSelector(minuteSelect),
        direction: useSelector(directionSelect),
        recycle: useSelector(recycleSelect)
    }
    const hasRecicle = () => {
        const { oil, paper, clothes, batteries, glass } = pickUp.recycle;
        if((oil || paper || clothes || batteries || glass) > 0){
           return true;
        }else{
            return false;
        }
    }

    const [step, setStep] = useState(1);
    const siguiente = 'Siguiente';
    const confirmar = 'Confirmar';
    
    const getSteps = step =>{
        if(step < 1 || step > 5) setStep(1);
        if(step === 1) return <PickUpStep1/>
        if(step === 2) return <PickUpStep2/>
        if(step === 3) {
            if(!pickUp.direction.longitude){
                setStep(2)
            }else{
            return <PickUpStep3/>}
        } 
        if(step === 4) {
            const someToRecicle = hasRecicle();
            if(someToRecicle){
                return <PickUpStep4/>}
            else{
                setStep(3);
            }
        }
    }
    const backToHome = async() => {
        props.history.push('/');
        await savePickUpService(pickUp)
        dispatch(activeModal({origin: 'pickUpForm'}));
    }

    // haciendo una ruta segura
    if(hasUser === null){
        return <div>
            ...loading
        </div>
    }

    if(hasUser === false){
        props.history.push('/');
    }

    return(
        <div className="b-pickup">
            <Stepper step={step} />
            <div  className="b-pickup__left">
                <span className="b-pickup__iconSize" id="Add-hour" onClick={()=> setStep(step - 1)}>
                    <FontAwesomeIcon  className="b-hour__icon" icon={faChevronLeft} />
                </span>
            </div>
            {getSteps(step)}
        
            <button className="b-button" onClick={step === 4 ? backToHome : ()=> setStep(step + 1)}>
                {step === 4 ? confirmar : siguiente}
            </button>
        </div>
    )
}

export default PickUpGeneral;

