import React from 'react';
//import Navbar from '../../containers/Navbar/Navbar';
import logoIcon from '../../Images/reciclo-logo-08.svg';
import oIcon from '../../Images/reciclo-O.png';
import './Splash.scss';

export default function Splash() {


    return(
        <div className="splash">
            
                <img
                    src={logoIcon}
                    alt="Logo-Reciclo"
                    className="splash__icon"
                />

                <img
                    src={oIcon}
                    alt="Logo-O"
                    className="splash__icon2"
                />
            

    
        </div>
    )

}