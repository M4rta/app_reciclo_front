import { useDispatch, useSelector} from 'react-redux';
import { desactiveModal, modalSelect, modalMessage1Select, message1Select ,
        modalMessage2Select, message2Select, modalMessage3Select, message3Select,
        chariot } from '../../slices/modalSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faCircle, } from '@fortawesome/free-solid-svg-icons';
import './Modal.scss';
import { withRouter } from 'react-router-dom';


const Modal = (props) => {

    const modal = useSelector(modalSelect);
    const message1 = useSelector(message1Select);
    const message2 = useSelector(message2Select);
    const message3 = useSelector(message3Select);
    const modalRecycle = useSelector(modalMessage2Select)
    const modalPickup = useSelector(modalMessage1Select);
    const modalDonation = useSelector(modalMessage3Select);
   
    const dispatch = useDispatch();
    const { part1, part2 } = message1;
    const { part3, part4 } = message2;
    const { part5, part6 } = message3;

    const closeModal = () => {
        if (modalRecycle){
            dispatch(desactiveModal())
            dispatch(chariot(false));
            props.history.push('/map');
        }else if (modalDonation){
            dispatch(desactiveModal())
            props.history.push('/donate');
        }else {
            dispatch(chariot(true));
            dispatch(desactiveModal())}
        
    }
    return(
     
        <div className={modal ? "generic-modal-wrapper generic-modal-wrapper--opened " : "generic-modal-wrapper"}>
            <div className="generic-modal-window">
                <div className="generic-modal-window-message1">
                    {modalPickup && <>
                     <FontAwesomeIcon  className="b-modal__icon" icon={faCheckCircle} />
                     <span>{part1}</span>
                     </>}
                    {modalRecycle && <>
                     <FontAwesomeIcon  className="b-modal__icon" icon={faCheckCircle} />
                     <span>{part3}</span>
                     </>}
                     {modalDonation && <>
                     <FontAwesomeIcon  className="b-modal__icon" icon={faCheckCircle} />
                     <span>{part5}</span>
                     </>}
                </div>
                <div className="generic-modal-window-message2">
                {modalPickup && <>
                     <FontAwesomeIcon  className="b-modal__icon--circle" icon={faCircle} />
                     <span>{part2}</span>
                     </>}
                {modalRecycle && <>
                     <FontAwesomeIcon  className="b-modal__icon--circle" icon={faCircle} />
                     <span>{part4}</span>
                     </>}
                {modalDonation && <>
                     <FontAwesomeIcon  className="b-modal__icon--circle" icon={faCircle} />
                     <span>{part6}</span>
                </>}
                </div>
                <button className="b-modal__button" onClick={closeModal}>{modalRecycle ? 'Ver mapa' : 'Aceptar'}</button>
            </div>
        </div>
        
    )
}

export default withRouter(Modal);