import "./Donate.scss";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getDonationsService, hasDonated, getDonationsByIdService} from "../../services/donations";
import { BrowserRouter as Router, useHistory, Link } from "react-router-dom";
import GoBack from "../../Images/reciclo-goBack.png";
import Lottie from '../Lottie/Lottie';
import {
  donationChooseSelect,
  donationSelect, 
  saveDonation,
  donationChoose
} from "../../slices/donationSlice";
import ButtonShadow from "../../Images/button-close-dropdown.png";

const Donate = (props) => {
  const donation = useSelector(donationSelect);
  const selectDonation = useSelector(donationChooseSelect);
  const [hasBeenDonated, setHasBeenDonated] = useState(false)
  const [allDonations, setAllDonations] = useState([]);
  const { title, description, photo, target, collected, _id } = donation;
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    downloadDonations();
    alreadyDonated();
    },[]);

  const downloadDonations = async () => {
    try {
      const donations = await getDonationsService();
      setAllDonations(donations);
    } catch (error) {
      console.log(error);
    }
  };

  const alreadyDonated = async() => {
    const donated = await hasDonated(_id);
    setHasBeenDonated(donated.donated);
  } 
  
  const handleHistory = () => {
    dispatch(donationChoose(false))
    history.push("/");
  };

  const showdetail = async (event) => {
    const { id } = event.target;
    const data = await getDonationsByIdService(id);
    dispatch(saveDonation(data));
    const donated = await hasDonated(data._id);
    setHasBeenDonated(donated.donated);
  }

  return (
      <div className="donate">
        <div className="donate__btn-goback">
          <button onClick={handleHistory} className="donate__btn-goback__btn">
            <span>
              <img
                src={GoBack}
                alt="Atrás"
                className="donate__btn-goback__btn__span"
              />
            </span>
            Atrás
          </button>
        </div>

        {selectDonation && (
          <div className="donate__card">
            <h2 className="donate__card__title">{title}</h2>
            <div className="donate__card__white">
              <div className="donate__card__lottie">
                {hasBeenDonated && <div className="b-lottie"><Lottie/></div>}
                <img
                  src={photo}
                  alt="Imagen card"
                  className="donate__card__white__img"
                />
              </div>
              <p className="donate__card__white__description">{description}</p>
              <h3 className="donate__card__white__donation">
                <span className="donate__card__white__donation__span">
                  {collected}€
                </span>{" "}
                recaudados del objetivo de{" "}
                <span className="donate__card__white__donation__span">
                  {target}€
                </span>
              </h3>
              
                <div className="donate__card__white__button-donate">
                <Link to="/form-donation">
                  <button className="donate__card__white__button-donate__btn">
                    Donar ahora
                  </button>
                </Link>
                </div>
              
              <span className="donate__card__white__shadow-button">
                <img
                  src={ButtonShadow}
                  alt="Close window"
                  className="donate__card__white__shadow-button__btn"
                />
              </span>
            </div>
          </div>
        )}

        {!selectDonation && (
          <div className="donate__light-bulb">
            <h2 className="donate__light-bulb__h2">
              ¿Dónde aportar mi donación?
            </h2>
            {allDonations.length &&
              allDonations.map((data) => (
                
                <div className="donate__light-bulb__card" key={data._id} onClick={showdetail} id={data._id} >
                  <img
                    id={data._id}
                    src={data.photo}
                    alt={data.alt}
                    className="donate__light-bulb__card__img"
                  />
                  <div className="donate__light-bulb__card__img__div">
                  {/* <Link to="/form-donation"> */}
                    <h3 className="donate__light-bulb__card__img__div__h3">
                      {data.title}
                    </h3>
                  {/* </Link> */}
                  </div>
                </div>
             
              ))}
          </div>
        )}
      </div>
    

    //poner bien los estilos -> HECHO, DE LA CARD
    //Botón goback en la card -> HECHO
    //HACER QUE EN LA BOMBILLA APAREZCAN TODAS LAS CARDS -> HECHO
    //hacer un botón que lleve al formulario.
    //hacer el form
    //en el onSubmit tiene que llamar a un servicio y pasar el amount y la id
    //de la donación.

    // http://localhost:4000/donations/donate/200/602ab6af5fb1d73f14b26a7d
  );
};

export default Donate;
