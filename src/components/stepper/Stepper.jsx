import { useEffect } from 'react';
import './Stepper.scss'
import "../../styles/Import.scss";

const Stepper = (props) => {

    useEffect(() => {
        changeStepColor();
   }, [props.step]);

    const changeStepColor = () =>{
        
        const stepper = document.getElementsByClassName('stepper__divIcon');
        for(let i = 0; i<stepper.length; i++){
            stepper[i].style.backgroundColor = "white";
        }
        for(let i = 0; i<=props.step-1; i++){
            stepper[i].style.backgroundColor = "#6e0b36";
        }
    }
    
    return(
        <div className="stepper__graph">
            <div className="stepper__divIcon"></div>
            <div className="stepper__divIcon"></div>
            <div className="stepper__divIcon"></div>
            <div className="stepper__divIcon"></div>
        </div>
    )
}

export default Stepper;