import React from 'react';
import HomeTop from '../../containers/HomeTop/HomeTop.jsx';
import ModalHome from '../../containers/ModalHome/ModalHome.jsx';
import Carrusel from '../../containers/Carrusel/Carrusel.jsx';
import TenloClaro from '../TenloClaro/TenloClaro.jsx';
import './Home.scss';

export default function Home() {

    return(
        <div className="home">
            <div className="test">
                <HomeTop/>
                <ModalHome/>
                <Carrusel/>
                <TenloClaro/>
            </div>
        </div> 
    )
}