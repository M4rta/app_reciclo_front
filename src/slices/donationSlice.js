import { createSlice } from '@reduxjs/toolkit';

export const donationSlice = createSlice({

    name: 'donation',
    initialState: {
        donation:{
            title: '',
            description:'',
            photo:'',
            target:'',
            collected:'',
            _id:'',   
        },
        donationChoose: false
    },
    reducers:{
        saveDonation: (state, action) => {
            const { title, description, photo, target, collected, _id } = action.payload;
            state.donation = {
                title,
                description,
                photo,
                target,
                collected,
                _id
            };
            state.donationChoose = true;
        },
        donationChoose: (state, action) => {
            state.donationChoose = action.payload;
        }
    }
})
export const { saveDonation, donationChoose } = donationSlice.actions;

export const donationSelect = state => state.donation.donation;
export const donationChooseSelect = state => state.donation.donationChoose;
export default donationSlice.reducer;