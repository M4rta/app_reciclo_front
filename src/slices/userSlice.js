import { createSlice } from '@reduxjs/toolkit';

export const userSlice = createSlice({
    
    name: 'user',
    initialState: {
        hasUser: null,
        user : null,
    },
    reducers:{
        setUserSlice: (state, action ) => {
            state.user = action.payload.user;
            state.hasUser = action.payload.hasUser;
        },
    }
})
export const { setUserSlice, checkSessionSlice} = userSlice.actions;

export const hasUserSelect = state => state.user.hasUser;
export const userSelect = state => state.user.user;

export default userSlice.reducer;