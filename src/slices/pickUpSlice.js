import { createSlice, current} from '@reduxjs/toolkit';

export const pickUpSlice = createSlice({

    name: 'pickUp',
    initialState: {
        date: new Date(),
        hour: 9,
        minute: 0,
        direction: {
            latitude: 0,
            longitude: 0,
            postal_code: '',
            locality: '',
            label: ''
        },
        recycle: {
            glass: 0,
            paper: 0,
            batteries: 0,
            clothes: 0,
            oil: 0
        }
    },
    reducers:{
        addHoursSlice: (state, action) => {
            const {id} = action.payload;
            const currentState = current(state);
            if(id === 'Add-hour'){
                if(state.hour===19){
                    state.hour = 9;
                }else{
                state.hour = currentState.hour + 1;
                }
             
            }else{
                if(state.hour===9){
                    state.hour = 19;
                }else{
                state.hour = currentState.hour - 1
                }
            }
        },
        addMinutesSlice: (state, action) => {
            const {id} = action.payload;
            if(id === 'Add-minutes'){
                if(state.minute===55){
                    state.minute=0;
                }else{
                state.minute = state.minute + 5;
                }
            }else{
                if(state.minute===0){
                    state.minute = 55;
                }else{
                state.minute = state.minute - 5;
                }
            }
        },
        getCalendarSlice: (state, action) => {
            state.date = action.payload;
        },
        addDirection: (state, action) => {
            state.direction = action.payload
        },
        addRecycle: (state, action) => {
      
            state.recycle = action.payload;

        }
    }
})
export const { addHoursSlice, addMinutesSlice, 
                getCalendarSlice, addDirection, addRecycle} = pickUpSlice.actions;

export const dateSelect = state => state.pickUp.date;
export const hourSelect = state => state.pickUp.hour;
export const minuteSelect = state => state.pickUp.minute;
export const recycleSelect = state => state.pickUp.recycle;
export const directionSelect = state => state.pickUp.direction;

export default pickUpSlice.reducer;