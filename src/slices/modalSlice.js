import { createSlice } from '@reduxjs/toolkit';

export const modalSlice = createSlice({

    name: 'modal',
    initialState: {
      modal: false,
      chariot:false,
      modalMessage1: false,
      modalMessage2: false,
      modalMessage3: false,
      message1: 
      {
        part1: 'Tu recogida ha sido programada con éxito!',
        part2: 'Te avisaremos cuando llegue la hora',
      },
      message2:
      {
        part3: 'Estamos de camino!',
        part4: 'Ten preparado tu reciclaje!'
      },
      message3:

      {
        part5: '¡Tu donación se ha efectuado con éxito!',
        part6: 'Enviaremos el certificado a tu email.'
      }
     },
    reducers:{

        activeModal: (state, action) => {
          const { origin } = action.payload;
          state.modal = true;
          if (origin === 'pickUpForm'){
             state.modalMessage1 = true;
          }else if (origin === 'pickUpRecycle'){
             state.modalMessage2 = true;
          }else if(origin === 'donation'){
            state.modalMessage3 = true;
          }
        },
        
        desactiveModal: (state, action) => {
          state.modal = false;
          state.modalMessage1 = false;
          state.modalMessage2 = false;
          state.modalMessage3 = false;
        // state.chariot = true;
            
        },
        chariot: (state, action) => {
          state.chariot = action.payload;;
        }
    }
})
export const { activeModal, desactiveModal, chariot } = modalSlice.actions;

export const chariotSelect = state => state.modal.chariot
export const modalSelect = state => state.modal.modal;
export const message1Select = state => state.modal.message1;
export const message2Select = state => state.modal.message2;
export const message3Select = state => state.modal.message3;
export const modalMessage1Select = state => state.modal.modalMessage1;
export const modalMessage2Select = state => state.modal.modalMessage2;
export const modalMessage3Select = state => state.modal.modalMessage3;

export default modalSlice.reducer;