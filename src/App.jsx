import React from "react";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch, Route, withRouter } from "react-router-dom";
import { checkSession } from "./services/auth.js";
import Navbar from "./containers/Navbar/Navbar.jsx";
import Home from "./components/Home/Home.jsx";
import LogIn from "./containers/LogIn/LogIn.jsx";
import Register from "./containers/Register/Register.jsx";
import PickupGeneral from "./components/PickUpGeneral";
import Splash from "./components/Splash/Splash.jsx";
import Donate from "./components/Donate";
// import DonateCard from "./components/DonateCard/DonateCard.jsx";
import Maps from "./containers/Maps";
import FormDonation from './containers/FormDonation/FormDonation.jsx';
import "./App.scss";
import {  setUserSlice } from "./slices/userSlice.js";
import Modal from './components/Modal';
import { modalSelect } from './slices/modalSlice';

function App(props) {
  const [loading, setLoading] = useState(true);
  const modal = useSelector(modalSelect);
  const dispatch = useDispatch();

  const [error, setError] = useState("");
 
 useEffect(() => {
     setTimeout(() => {
        setLoading(false);
     }, 1 * 1000);
  
    checkUserSession();
  }, []);

  const checkUserSession = async () => {
    try {
      const data = await checkSession();
      delete data.password;
      if (data._id){
        dispatch(setUserSlice({user:data, hasUser: true}));
      }else{
        setError(error.message);
        dispatch(setUserSlice({user:null, hasUser: false}));
      }
    } catch (error) {
      setError(error.message);
      dispatch(setUserSlice({user: null, hasUser: false}));
    }
  };

  const saveUser = (user) => {
    delete user.password;
    setError("");
  };

  if (loading && props.location.pathname === "/") return <Splash />;

  return (
    <div className="App">
      <Switch>
        <Route
          exact
          path="/login"
          component={(props) => <LogIn {...props} saveUser={saveUser} />}
        />
        <Route
          exact
          path="/register"
          component={(props) => <Register {...props} saveUser={saveUser} />}
        />
        <Route exact path="/form-donation" component={FormDonation} />
        <Route exact path="/pickup" component={PickupGeneral} />
        <Route exact path="/donate" component={Donate} />
        {/* <Route exact path="/donate-card" component={DonateCard} /> */}
        <Route exact path="/map" component={Maps} />
        <Route exact path="/" component={Home} />
        
      </Switch>
        <Navbar />
      {modal && <div><Modal/></div>}

    </div>
  );
}

export default withRouter(App);
